import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
	public goodSelected = true;
	public badSelected = true;
  constructor(private vieCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
  	this.goodSelected = this.navParams.get('goodSelected');
  	this.badSelected = this.navParams.get('badSelected');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  closeModal(){
  	let filterState ={
  		goodSelected: this.goodSelected,
  		badSelected : this.badSelected
  	};
  	this.vieCtrl.dismiss(filterState);
  	// this.navCtrl.pop();
  }
}
