import { Component } from '@angular/core';
import { NavController, AlertController, reorderArray, ToastController } from 'ionic-angular';
import { ListPage } from '../list/list';

import { ListProvider } from '../../providers/list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public lists=[];
	public reorderIsEnabled=false;
	public newsTopage = ListPage;
	// public deletelists = [];
  constructor(private listService: ListProvider , private toastController: ToastController ,  public navCtrl: NavController, private alertController: AlertController) {
  	this.lists= this.listService.getDeleteList();
  	
  }

  deleteList(listIndex){
  	this.listService.deleteList(listIndex);
  }

  toggleReorder(){
  	this.reorderIsEnabled= !this.reorderIsEnabled;
  }

  ///geser list
  itemReorder($event){
  	reorderArray(this.lists, $event);
  }

  ///link page
  newspage(){
  	this.navCtrl.push(ListPage);
  }

  openTodoAlert(){
  	let addList = this.alertController.create({
  		title : "Add a todo list",
  		message : "Enter your todo",
  		inputs : [
  			{
  				type: "text",
  				name: "addListInput"
  			}
  		],
  		buttons:[
  			{
  				text: "Cancel"
  			},
  			{
  				text: "Add",
  				handler:(inputData)=>{
  					let listText;
  					listText = inputData.addListInput;
  					this.lists.push(listText); ///fungsi add

            ///alert pesan selesai add
  					addList.onDidDismiss(()=>{
    					let addListToast = this.toastController.create({
    						message : "List Added",
    						duration : 2000
    					});
    					addListToast.present();
              
            })
  				}
  			}
  		]
  	});
  	addList.present();
  }

  editList(listIndex){
    let editListAlert = this.alertController.create({
      title : "Edit list",
      message: "Enter new list",
      inputs:[
        {
          type: "text",
          name: "editListInput",
          value: this.lists[listIndex]
        }
      ],
      buttons:[
        {
          text: "Cancel"
        },
        {
          text: "Edit",
          handler: (inputData)=>{
            let listText;
            listText= inputData.editListInput;
            this.listService.editingList(listText, listIndex);

            let editListToast = this.toastController.create({
              message: "List Edited",
              duration : 2000
            });
            editListToast.present();
          }
        }
      ]
    });
    editListAlert.present();
  }

}
