import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { BestPage } from '../best/best';
import { ModalPage } from '../modal/modal';
import { ProductProvider } from '../../providers/product/product';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
	public bestSeller =[];
	public goodSelected = true;
	public badSelected = true;
  constructor(private productService: ProductProvider,public navCtrl: NavController, private modalCtrl : ModalController) {

  }

  ionViewDidLoad(){
  	this.productService.getProduct()
  		.subscribe(allProduct=>{
  			this.bestSeller = allProduct.filter(products=> products.best==true);
  			console.log(this.bestSeller);
  		})
  }

  detailPage(data){
  	this.navCtrl.push(BestPage,{item:data});
  }

  openFilter(){
  	let filterStateForPage ={
  		goodSelected: this.goodSelected,
  		badSelected : this.badSelected
  	};
  	let openModal = this.modalCtrl.create(ModalPage, filterStateForPage);
  	openModal.onDidDismiss((filterState)=>{
  		this.goodSelected = filterState.goodSelected;
  		this.badSelected = filterState.badSelected;
  		// console.log(filterState);
  		this.productService.getProduct()
  			.subscribe((bestSeller)=>{
  				let products = bestSeller;
  				if (filterState.goodSelected && filterState.badSelected) {
  					this.bestSeller = products.filter((product=>{
  					return product.best == true;
  					}));
  				} else if (!filterState.goodSelected && !filterState.badSelected) {
  					this.bestSeller =[];
  					return;
  				} else if (filterState.goodSelected && !filterState.badSelected) {
  					this.bestSeller =products.filter((product=>{
  					return product.status !== "bad" && product.best == true;
  					}));
  				} else {
  					this.bestSeller =products.filter((product=>{
  					return product.status !== "good"  && product.best == true;
  					}));
  				}
  			})
  	})
  	openModal.present();
  }
}
