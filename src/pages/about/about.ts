import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import "rxjs/add/operator/map";

import { ProductProvider } from '../../providers/product/product';
import { DetailPage } from '../detail/detail';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
	public products=[];
  constructor(private productService: ProductProvider, public navCtrl: NavController) {

  }
  ionViewDidLoad(){
  	this.productService.getProduct()
  		.subscribe(respone =>{
  			this.products = respone;
  		});
  }
  //link page dan mengambil data di parameter
  detailPage(item){
  	this.navCtrl.push(DetailPage, {xxx:item});
  }

}
