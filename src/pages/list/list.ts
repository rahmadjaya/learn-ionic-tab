import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListProvider } from '../../providers/list/list';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
	public deletelists = [];
  constructor(private listService: ListProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.deletelists = this.listService.getDeleteLists();
  }

}
