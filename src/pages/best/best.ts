import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the BestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-best',
  templateUrl: 'best.html',
})
export class BestPage {
	selectedDet :any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.selectedDet = navParams.get('item');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BestPage');
  }

}
