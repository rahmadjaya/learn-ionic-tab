import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ListProvider {
	private lists=[];
	private deletelists=[];
  constructor(public http: Http) {
  	//ambil datanya simpen di lists
  	// this.http.get('/assets/icon/data.json')
	  // 	.map(respone=>respone.json())
	  // 	.subscribe(data=>data);
	//console.log(respone)
    console.log('Hello ListProvider Provider');
    //console.log(data)

  }

  deleteList(listIndex){
  	let listToDelete = this.lists[listIndex];
  	this.lists.splice(listIndex, 1);
  	this.deletelists.push(listToDelete);
  }

  getDeleteList(){
  	return this.lists;
  }

  getDeleteLists(){
  	return this.deletelists;
  }

  editingList(list,listIndex){
    this.lists[listIndex]= list;
  }

}
